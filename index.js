"use strict"

// section two

const secTwoPar = document.querySelector('.sec_slides');
const secTwoBtn = document.querySelectorAll('.sec_two_btns');
const secTwoText = document.querySelectorAll('.sec_two_hidden');

secTwoPar.addEventListener('click', function showHidden(ev) {
  const triangle = document.querySelectorAll('.sec_two_triangle');

  secTwoBtn.forEach(i => {
    i.classList.remove('sec_two_active');
  })

  if (ev.target.classList.contains('sec_two_btns')) {
    ev.target.classList.toggle('sec_two_active');
  }

  secTwoText.forEach(i => {
    i.classList.remove('sec_two_text_active');
  })

  const id = ev.target.getAttribute('data-tab');
  document.querySelector(id).classList.add('sec_two_text_active');
});

// animationLoading

const loader = document.querySelector('.loader-wrap');

function startLoading() {
  loader.classList.add('active')
}

function endLoad() {
  loader.classList.remove('active');
}


// section three

const secThreePar = document.querySelector('.three_slides');

secThreePar.addEventListener('click', filter);
let target;

function filter(ev) {
  const secThreeBtns = document.querySelectorAll('.three_slide_list');
  const secThreePics = document.querySelectorAll('.sec_three_flex_wrap');

  secThreeBtns.forEach(i => {
    i.classList.remove('sec_three_active');
  })

  if (ev.target.classList.contains('three_slide_list')) {
    ev.target.classList.toggle('sec_three_active');
  }

  secThreePics.forEach(i => {
    i.classList.remove('sec_three_img_active')
  })

  const id = ev.target.getAttribute('id');

  id === 'all' ? secThreePics.forEach(i => i.classList.add('sec_three_img_active')) : document.querySelectorAll(`[data-product = "#${id}"]`).forEach(i => i.classList.add('sec_three_img_active'));

}
let allPics = document.getElementsByClassName('sec_three_flex_wrap');
const PicAdd = document.querySelector('.sec_three_btn');
PicAdd.addEventListener('click', function () {
  startLoading();
  if ((allPics.length === 12)) {
    setTimeout(endLoad, 5000);
    setTimeout(addPics, 5000);
  } else {
  setTimeout(endLoad, 5000);
  setTimeout(extraPics, 5500);
  }
});


function addPics(ev) {

  for (let i = 1; i <= 12; i++) {
    const div = document.createElement('div');
    const divHover = document.createElement('div');
    const img = document.createElement('img');
    div.classList.add('sec_three_flex_wrap', 'sec_three_img_active');
    divHover.classList.add('hover');
    img.setAttribute('src', `./img/sec_three_add_${[i]}.jpg`);
    img.classList.add('sec_three_flex_item');
    let depName;

    if (i >= 1 && i <= 3) {
      div.setAttribute('data-product', `#grapрDesign`);
      depName = 'Graphic Design';
    }
    if (i >= 4 && i <= 6) {
      div.setAttribute('data-product', `#webDesig`);
      depName = 'Web Designt';
    }
    if (i >= 7 && i <= 9) {
      div.setAttribute('data-product', `#landingPages`);
      depName = 'Landing Pages';
    }
    if (i >= 10 && i <= 12) {
      div.setAttribute('data-product', `#wordPress`);
      depName = 'Wordpress';
    }

    divHover.innerHTML = `<div class="hover_top">
    <div class="left_item"> <svg class="hover_item_left" width="15" height="15" viewBox="0 0 15 15"
        fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z"
          fill="#1FDAB5" />
      </svg></div>
    <div class="right_item">
      <div class="right_item_square"></div>
    </div>
  </div>
  <div class="hover_bottom">
    <p class="bottom_title">creative design</p>
    <p class="bottom_text">${depName}</p>
  </div>`;
    div.append(img, divHover);

    document.querySelector('.sec_three_flex').append(div);

    const secThreePics = document.querySelectorAll('.sec_three_flex_wrap');

    secThreePics.forEach(i => {
      i.classList.remove('sec_three_img_active')
    })

    const id = document.querySelector('.sec_three_active').getAttribute('id');
    id === 'all' ? secThreePics.forEach(i => i.classList.add('sec_three_img_active')) : document.querySelectorAll(`[data-product = "#${id}"]`).forEach(i => i.classList.add('sec_three_img_active'));
  }
}

function extraPics(ev) {
  for (let i = 1; i <= 12; i++) {
    const div = document.createElement('div');
    const divHover = document.createElement('div');
    const img = document.createElement('img');
    div.classList.add('sec_three_flex_wrap', 'sec_three_img_active');
    divHover.classList.add('hover');
    img.setAttribute('src', `./img/sec_three_extra_${[i]}.jpg`);
    img.classList.add('sec_three_flex_item');
    let depName;

    if (i >= 1 && i <= 3) {
      div.setAttribute('data-product', `#grapрDesign`);
      depName = 'Graphic Design';
    }
    if (i >= 4 && i <= 6) {
      div.setAttribute('data-product', `#webDesig`);
      depName = 'Web Designt';
    }
    if (i >= 7 && i <= 9) {
      div.setAttribute('data-product', `#landingPages`);
      depName = 'Landing Pages';
    }
    if (i >= 10 && i <= 12) {
      div.setAttribute('data-product', `#wordPress`);
      depName = 'Wordpress';
    }

    divHover.innerHTML = `<div class="hover_top">
          <div class="left_item"> <svg class="hover_item_left" width="15" height="15" viewBox="0 0 15 15"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" clip-rule="evenodd"
                d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z"
                fill="#1FDAB5" />
            </svg></div>
          <div class="right_item">
            <div class="right_item_square"></div>
          </div>
        </div>
        <div class="hover_bottom">
          <p class="bottom_title">creative design</p>
          <p class="bottom_text">${depName}</p>
        </div>`;
    div.append(img, divHover);

    document.querySelector('.sec_three_flex').append(div);

    const secThreePics = document.querySelectorAll('.sec_three_flex_wrap');

    secThreePics.forEach(i => {
      i.classList.remove('sec_three_img_active')
    })

    let id = document.querySelector('.sec_three_active').getAttribute('id');
    id === 'all' ? secThreePics.forEach(i => i.classList.add('sec_three_img_active')) : document.querySelectorAll(`[data-product = "#${id}"]`).forEach(i => i.classList.add('sec_three_img_active'));

  }
  setTimeout(btnDisable, 1000);
  function btnDisable() {
    PicAdd.style.display = 'none';
  }
}


// section five

const secFivePar = document.querySelector('.sec_five_list');
const secFiveProfiles = document.querySelectorAll('.sec_five_slider_item');
const secFivePicts = document.querySelectorAll('.sec_five_pic');
let count = 1;
let activeProf;

secFivePar.addEventListener('click', function (ev) {
  if (ev.target.classList.contains('sec_five_pic')) {
    secFiveProfiles.forEach(i => {
      i.classList.remove('sec_five_slider_active');
    })

    secFivePicts.forEach(i => {
      i.classList.remove('pic_move');
    })
    ev.target.classList.add('pic_move');
    const id = ev.target.getAttribute('data-profile');
    document.querySelector(id).classList.add('sec_five_slider_active');
    count = ev.target.dataset.profile.split("-")[1];
  }
})

const backBtn = document.querySelector('.move_left');
const nextBtn = document.querySelector('.move_right');

nextBtn.addEventListener("click", function (ev) {
  if (+count === 4) {
    count = 1;
  } else {
    count++;
  }

  document.querySelector(".sec_five_slider_active").classList.remove("sec_five_slider_active");
  document.querySelector(".pic_move").classList.remove("pic_move");

  let blockToShow = document.querySelector(`#block-${count}`);
  blockToShow.classList.add("sec_five_slider_active");
  let slideImgUp = document.querySelector(`[data-profile="#block-${count}"]`);
  slideImgUp.classList.add("pic_move");
});

backBtn.addEventListener("click", function (ev) {
  if (+count === 1) {
    count = 4;
  } else {
    count--;
  }

  document.querySelector(".sec_five_slider_active").classList.remove("sec_five_slider_active");
  document.querySelector(".pic_move").classList.remove("pic_move");

  let blockToShow = document.querySelector(`#block-${count}`);
  blockToShow.classList.add("sec_five_slider_active");
  let slideImgUp = document.querySelector(`[data-profile="#block-${count}"]`);
  slideImgUp.classList.add("pic_move");
})

// section_six

let $container = $(".masonry-container");
$container.imagesLoaded(function () {
  $container.masonry({
    itemSelector: ".item_masonry",
    columnWidth: ".item_masonry",
    gutter: 20,
  });

  let msnryInner = $(".inner_masonry").masonry({
    itemSelector: ".inn_masonry",
    gutter: 3,
  });

  let msnryInnerLittle = $(".small_inner").masonry({
    itemSelector: ".inner",
    gutter: 3,
  });
});

$(document).ready(masonryHover);

function masonryHover() {
  $(".item_masonry").hover(
    function (ev) {
      $(this).find('.cover_item_gallery').fadeIn();
    },
    function () {
      $(this).find('.cover_item_gallery').fadeOut();
    }
  );

  $(".large_inner").hover(
    function (ev) {
      {
        $(this).find('.cover_item_gallery').fadeIn();
      }
      ev.stopPropagation();
    },
    function () {
      $(this).find('.cover_item_gallery').fadeOut();
    }
  );

  $(".inner").hover(
    function (ev) {
      $(this).find('.cover_item_gallery').fadeIn();
      ev.stopPropagation();
    },
    function () {
      $(this).find('.cover_item_gallery').fadeOut();
    }
  );
};

const $masonryAddBtn = document.querySelector('.sec_six_btn');
const masonryLoadingWrap = document.querySelector('.masonry-loader-wrap');

function startMasonryLoad() {
  masonryLoadingWrap.classList.add('active');
}

function endMasonryLoad() {
  masonryLoadingWrap.classList.remove('active');
}

$masonryAddBtn.addEventListener('click', function (ev) {
  startMasonryLoad();
  setTimeout(endMasonryLoad, 5000);
  setTimeout(addImageMasonry, 5500);
});



function addImageMasonry() {
  for (let i = 1; i <= 8; i++) {
    let loadDiv = document.createElement("div");
    loadDiv.classList.add("item_masonry");
    $(loadDiv).html(`        <img src="./img/sec_six_add_${i}.jpg" alt="house">
    <div class="cover_item_gallery">
      <a href="#" class="icon_tools search_icon"><svg width="10" height="10" viewBox="0 0 10 10" fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path d="M5.37262 5.66669L9 9" stroke="white" />
          <circle cx="3.5" cy="3.5" r="3" stroke="white" />
        </svg>
      </a>
      <a href="#" class="icon_tools zoom_icon"><svg width="12" height="11" viewBox="0 0 12 11" fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path
            d="M0.700061 10C0.700061 10.1657 0.834376 10.3 1.00006 10.3L3.70006 10.3C3.86575 10.3 4.00006 10.1657 4.00006 10C4.00006 9.83431 3.86575 9.7 3.70006 9.7H1.30006V7.3C1.30006 7.13431 1.16575 7 1.00006 7C0.834376 7 0.700061 7.13431 0.700061 7.3L0.700061 10ZM3.78793 6.78787L0.787929 9.78787L1.21219 10.2121L4.21219 7.21213L3.78793 6.78787Z"
            fill="#F8FCFE" />
          <path
            d="M0.999864 0.700202C0.834179 0.700267 0.699917 0.834634 0.699982 1.00032L0.701042 3.70032C0.701107 3.866 0.835474 4.00027 1.00116 4.0002C1.16685 4.00014 1.30111 3.86577 1.30104 3.70008L1.3001 1.30008L3.7001 1.29914C3.86579 1.29908 4.00005 1.16471 3.99998 0.999024C3.99992 0.833338 3.86555 0.699077 3.69986 0.699141L0.999864 0.700202ZM4.21321 3.78681L1.21203 0.787986L0.787933 1.21242L3.78911 4.21124L4.21321 3.78681Z"
            fill="#F8FCFE" />
          <path
            d="M11.3001 1C11.3001 0.834315 11.1657 0.7 11.0001 0.7L8.30006 0.7C8.13438 0.7 8.00006 0.834315 8.00006 1C8.00006 1.16569 8.13438 1.3 8.30006 1.3H10.7001V3.7C10.7001 3.86569 10.8344 4 11.0001 4C11.1657 4 11.3001 3.86569 11.3001 3.7L11.3001 1ZM8.21219 4.21213L11.2122 1.21213L10.7879 0.787868L7.78793 3.78787L8.21219 4.21213Z"
            fill="#F8FCFE" />
          <path
            d="M11.0297 10.2727C11.1954 10.2712 11.3285 10.1357 11.327 9.97001L11.3026 7.27012C11.3011 7.10445 11.1656 6.97135 10.9999 6.97285C10.8342 6.97435 10.7011 7.10988 10.7026 7.27555L10.7243 9.67546L8.32442 9.69717C8.15874 9.69867 8.02565 9.8342 8.02715 9.99988C8.02865 10.1656 8.16417 10.2986 8.32985 10.2971L11.0297 10.2727ZM7.7898 7.21404L10.8168 10.1868L11.2372 9.75869L8.2102 6.78596L7.7898 7.21404Z"
            fill="#F8FCFE" />
        </svg>
      </a>
    </div>`);
    $(".masonry-container").append(loadDiv);
  }

  let container = document.querySelector(".masonry-container");
  let msnry;

  imagesLoaded(container, function () {
    msnry = new Masonry(container, {
      itemSelector: ".item_masonry",
      columnWidth: ".item_masonry",
      gutter: 20,
    });
    masonryHover();
  });

  setTimeout(btnDisAble, 1000);
  function btnDisAble() {
    $masonryAddBtn.style.display = 'none';
  }
}
